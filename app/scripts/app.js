$(document).ready(function() {
    $('a[href="#"]').click(function(e){
        e.preventDefault();
    });

    // Select
    $('select').selectric();

    // Range slider
    // http://ionden.com/a/plugins/ion.rangeSlider/en.html
    $("#example_id").ionRangeSlider();

    // Carousels
    function responsiveCarousel() {
      $('.initial-responsive-carousel').on('init', function(event, slick, direction){
        $(this).addClass('initialed');
      });
      $('.initial-responsive-carousel').on('destroy', function(event, slick, direction){
        $(this).removeClass('initialed');
      });
      $('.initial-responsive-carousel').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        arrows: true,
        swipeToSlide: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              }
            }
          ]
      });
    };
    responsiveCarousel();

    function noneResponsiveCarousel() {
      $('.initial-none-responsive-carousel').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        arrows: true,
        swipeToSlide: true,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              }
            },
            {
              breakpoint: 568,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: '<button type="button" class="slick-prev slick-arrow-mobile"></button>',
                nextArrow: '<button type="button" class="slick-next slick-arrow-mobile"></button>'
              }
            }
          ]
      });
    };
    noneResponsiveCarousel();

    function objectCarousel() {
      $('.object-carousel-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        fade: true,
        asNavFor: '.object-carousel-thumbs',
        responsive: [
            {
              breakpoint: 768,
              settings: {
                dots: false
              }
            },
            {
              breakpoint: 568,
              settings: {
                arrows: false,
                dots: false
              }
            },
          ]
      });
      $('.object-carousel-thumbs').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.object-carousel-single',
        dots: false,
        arrows: false,
        centerMode: false,
        focusOnSelect: true,
        swipeToSlide: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 420,
              settings: {
                slidesToShow: 3
              }
            }
          ]
      });
    };
    objectCarousel();
    // end Carousels


    // Modal windows
    function modal() {
      $('.popup-with-zoom-anim').magnificPopup({
          type: 'inline',

          fixedContentPos: false,
          fixedBgPos: true,

          overflowY: 'auto',

          closeBtnInside: true,
          preloader: false,

          midClick: true,
          removalDelay: 300,
          mainClass: 'my-mfp-zoom-in',
          callbacks: {
              open: function() {
                $('.modal form input').on('focus', function(){
                  $(this).parent().addClass('focused');
                });
                $('.modal form input').on('blur', function(){
                  $(this).parent().removeClass('focused');
                });
              }
            }
        });
    };
    modal();


    // Animation for inputs
    function inputs() {
      $('.contacts-form input, .contacts-form textarea').on('focus', function(){
        $(this).parent().addClass('focused');
      });
      $('.contacts-form input, .contacts-form textarea').on('blur', function(){
        $(this).parent().removeClass('focused');
      });
    };
    inputs();

    // Trigger button for mobile menu
    function mobileMenu() {
      var trigger = $('.mobile-menu a');
      var menu = $('.m_header .nav');
      var layout = $('.body-layout');

      trigger.on('click', function(){
        menu.addClass('open');
        layout.addClass('is-active');
      });

      layout.on('click', function(){
        menu.removeClass('open');
        layout.removeClass('is-active');
      });
    };
    mobileMenu();


    // Map: contacts
    function contactsMap() {
      var map;
      var coordX = $('#map').data('coord-x');
      var coordY = $('#map').data('coord-y');

      function initialize()
      {
        map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(coordX,coordY),
          zoom: 16,
          scrollwheel: false,
           navigationControl: false,
           mapTypeControl: false,
           scaleControl: false,
           mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        setMarkers(map);

      };
      var places = [
        ['Moscow1', coordX, coordY, 1]
      ];
      function setMarkers(map) {
        var image = {
          url: '../images/map.png',
          size: new google.maps.Size(19, 29),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(8, 29)
        };

        for (var i = 0; i < places.length; i++) {
          var place = places[i];
          var marker = new google.maps.Marker({
            position: {lat: place[1], lng: place[2]},
            map: map,
            icon: image,
            title: place[0],
            zIndex: place[3]
          });
        }
      };
      google.maps.event.addDomListener(window, 'load', initialize);
    };

    if ( $('#map').length > 0 ) {
      contactsMap();
    }

    // Map: catalog
    function catalogMap() {
      var map;
      var coordX = $('#mapcatalog').data('coord-center-x');
      var coordY = $('#mapcatalog').data('coord-center-y');

      function initialize()
      {
        map = new google.maps.Map(document.getElementById('mapcatalog'), {
          center: new google.maps.LatLng(coordX,coordY),
          zoom: 9,
          scrollwheel: false,
           navigationControl: false,
           mapTypeControl: false,
           scaleControl: false,
           mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        setMarkers(map);

      };
      var places = [
        ['Some name 1', 55.571372, 37.417625, 3],
        ['Some name 2', 55.671372, 37.427625, 2],
        ['Some name 3', 55.771372, 37.537625, 1]
      ];
      function setMarkers(map) {
        var image = {
          url: '../images/map.png',
          size: new google.maps.Size(19, 29),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(8, 29)
        };

        for (var i = 0; i < places.length; i++) {
          var place = places[i];
          var marker = new google.maps.Marker({
            position: {lat: place[1], lng: place[2]},
            map: map,
            icon: image,
            title: place[0],
            zIndex: place[3]
          });
        }
      };
      google.maps.event.addDomListener(window, 'load', initialize);
    };

    if ( $('#mapcatalog').length > 0 ) {
      catalogMap();
    }



    // Media queries in JS
    $.breakpoint({
        condition: function () {
            return window.matchMedia('only screen and (max-width:568px)').matches;
        },
        first_enter: function () {
          console.log('initial');
        },
        enter: function () {
            console.log('enter');
            $('.initial-responsive-carousel').slick('unslick');
        },
        exit: function () {
          console.log('exit');
          if ( !$('.initial-responsive-carousel').hasClass('initialed') ) {
            responsiveCarousel();
          }
        }
    });
});

